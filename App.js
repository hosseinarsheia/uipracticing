import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Header } from 'react-native-elements'
import { Dropdown } from 'react-native-material-dropdown'
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import { observer, Provider } from 'mobx-react'
import { createFluidNavigator } from 'react-navigation-fluid-transitions'

import Mainscreen from './src/screens/MainScreen'
import MarsPicScreen1 from './src/screens/FluidTransition/MarsPicScreen1'
import MarsPicScreen2 from './src/screens/FluidTransition/MarsPicScreen2'
import MarsPicScreen3 from './src/screens/FluidTransition/MarsPicScreen3'
import MarsPicScreen4 from './src/screens/FluidTransition/MarsPicScreen4'

import R from './src/res/R'
import MyCalendar from './src/components/MyCalendar'
import ActionBar from './src/components/ActionBar'
import Navigation from './src/utils/Navigation'

global.appVersion = 32
global.appVersionName = '1.4.0'

console.disableYellowBox = true

global.DebugLog = (text, addBeginSeparator = false, addEndSeparator = false) => {
  if (__DEV__) {
    if (addBeginSeparator) console.log(`-----------------------`)
    console.log(text)
    if (addEndSeparator) console.log(`-----------------------`)
  }
}

const AppNavigator = createFluidNavigator(
  {
    Mainscreen: Mainscreen,
    MarsPicScreen1: MarsPicScreen1,
    MarsPicScreen2: MarsPicScreen2,
    MarsPicScreen3: MarsPicScreen3,
    MarsPicScreen4: MarsPicScreen4,
  },
  {
    initialRouteName: 'MarsPicScreen2', //TODO: set to SplashScreen on release
    headerMode: 'none',
    mode: 'modal',
    // SplashScreen
    // MainScreen
    // LoginScreen
  },
)

const AppContainer = createAppContainer(AppNavigator)

class App extends Component {
  render() {
    let data = [{ value: 'Banana' }, { value: 'Mango' }, { value: 'Pear' }]

    return (
      <Provider>
        <AppContainer ref={(navigatorRef) => Navigation.setTopLevelNavigator(navigatorRef)} />
      </Provider>
    )
  }
}

export default App

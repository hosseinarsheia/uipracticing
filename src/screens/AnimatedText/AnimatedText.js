import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Button } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'

import R from '../../res/R'
import TextAnimatoorComponent from './TextAnimatoorComponent'

const textContent = 'For the things we have to learn before we can do them, we learn by doing them. React Native.  '

@observer
class AnimatedText extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
        <TextAnimatoorComponent content={textContent} />
      </View>
    )
  }
}

export default AnimatedText

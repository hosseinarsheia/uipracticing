import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Button, Animated } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'

import R from '../../res/R'

@observer
class TextAnimatoorComponent extends Component {
  animatedValue = []

  constructor(props) {
    super(props)
    const textArr = props.content.trim().split(' ')

    textArr.forEach((_, i) => {
      this.animatedValue[i] = new Animated.Value(0)
    })
    this.textArr = textArr
  }

  componentDidMount = () => {
    this.animated()
  }

  animated = (toValue = 1) => {
    const animations = this.textArr.map((_, i) => {
      return Animated.timing(this.animatedValue[i], {
        toValue,
        duration: 300,
        useNativeDriver: true,
      })
    })

    Animated.stagger(100, animations).start()
  }

  render() {
    const { textStyle, style, content } = this.props
    return (
      <View style={[style, styles.textContainer]}>
        {this.textArr.map((text, index) => (
          <Animated.Text
            key={index}
            style={[
              textStyle,
              {
                opacity: this.animatedValue[index],

                // transform: [{ translateY: Animated.multiply(this.animatedValue[index], -50) }],
              },
            ]}>
            {text}{' '}
          </Animated.Text>
        ))}
      </View>
    )
  }
}

export default TextAnimatoorComponent

export const styles = StyleSheet.create({
  textContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
})

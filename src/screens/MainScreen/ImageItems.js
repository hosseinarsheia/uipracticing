import React, { useEffect, Fragment } from 'react'
import { View, Platform, Text, Alert, Image, StyleSheet } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { Avatar, Icon } from 'react-native-elements'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'

import R from '../../res/R'

const ImageItems = ({ item }) => {
  let image = item.image
  return (
    <Fragment>
      <View style={styles.container}>
        <View style={styles.firstColumn}>
          <Image style={{ width: null, height: null, flex: 1 }} source={item.image} resizeMode="cover" />
        </View>

        <View style={styles.secondColumn}>
          <Text style={styles.titleText}>{item.title}</Text>

          <Text numberOfLines={3} style={[styles.bodyText, { paddingRight: R.dimensions.h15 }]}>
            {item.body}
          </Text>

          <View style={styles.bottomRowWrapper}>
            <Text style={styles.priceTextStyle}>{item.price}</Text>
            <Text style={styles.weightTextStyle}>{item.weight}</Text>
            <Icon
              containerStyle={{ marginLeft: scale(75) }}
              name="plus-circle"
              type="material-community"
              color="#ff3c00"
              size={R.dimensions.b25}
            />
          </View>
        </View>
      </View>
      <View style={styles.border} />
    </Fragment>
  )
}

export default observer(ImageItems)

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    marginHorizontal: scale(16),
    marginVertical: verticalScale(10),
  },
  firstColumn: {
    flexDirection: 'row',
    height: '100%',
    width: scale(112.5),
  },
  secondColumn: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginLeft: scale(14),
    flex: 1,
  },
  titleText: {
    fontFamily: R.fonts.NunitoSans_ExtraBold,
    fontSize: moderateScale(15),
    fontWeight: '900',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#212f49',
  },
  bodyText: {
    fontFamily: R.fonts.NunitoSans_Regular,
    fontSize: moderateScale(12),
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    // textAlign: 'left',
    color: '#6d6d6d',
    marginRight: scale(16),
  },
  border: {
    borderBottomWidth: 1,
    borderColor: R.colors.borderColor,
    marginVertical: verticalScale(10),
    marginHorizontal: R.dimensions.h15,
  },
  bottomRowWrapper: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    marginTop: R.dimensions.v5,
  },
  priceTextStyle: {
    ...R.styles.normalFont,
    fontWeight: '700',
    fontSize: R.fontSizes.fs15,
    color: '#ff3c00',
  },
  weightTextStyle: {
    ...R.styles.normalFont,
    color: '#212f49',
    fontWeight: '700',
    marginLeft: scale(25),
    fontSize: R.fontSizes.fs15,
  },
})

// {
//     title: 'Spaghetti alla puttanesca',
//     body:
//       'Spaghetti alla puttanesca is an Italian pasta dish invented in Naples in the mid-20th century. Its ingredients typically include tomatoes, olive oil, anchovies, olives, capers and garlic.',
//     price: '$19',
//     weight: '300 gr.',
//     image: '../../images/food1.jpg',
//   },

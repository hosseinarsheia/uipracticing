import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, FlatList, SafeAreaView, Platform, StatusBar } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { scale, verticalScale, moderateScale } from 'react-native-size-matters'
import { Icon } from 'react-native-elements'

import R from '../../res/R'
import ActionBar from '../../components/ActionBar'
import ImageItems from './ImageItems'
import { ScrollView } from 'react-native-gesture-handler'

class Mainscreen extends Component {
  ImageItems = ({ item }) => {
    return <ImageItems item={item} />
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center' }}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View
          style={{
            width: '100%',
            height: Platform.OS == 'ios' ? verticalScale(24) : 0,
            backgroundColor: 'white',
          }}></View>

        <View
          style={{
            width: '100%',
            height: verticalScale(56),
            flexDirection: 'row',
            borderBottomWidth: 1,
            alignItems: 'center',
          }}>
          <Icon
            name="keyboard-backspace"
            type="material-community"
            color="#212f49"
            size={R.dimensions.b25}
            containerStyle={{
              height: '100%',
              justifyContent: 'center',
              marginLeft: scale(16),
            }}
          />

          <Text style={styles.headerTextStyle}>Maggiore f Pizza Delivery</Text>

          <Icon
            name="info"
            type="feather"
            color="#ff3c00"
            size={R.dimensions.b25}
            containerStyle={{ height: '100%', justifyContent: 'center', marginRight: scale(16) }}
          />
        </View>

        <View
          style={{
            width: '100%',
            alignItems: 'center',
            elevation: 7,
            backgroundColor: 'white',
            shadowColor: 'black',
            shadowOffset: {
              width: 5,
              height: 5,
            },
            shadowOpacity: 0.2,
            shadowRadius: 7,
          }}>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ alignItems: 'center' }}>
            {headertextList.map((text, index) => (
              <View
                style={{
                  paddingVertical: R.dimensions.v15,
                  paddingHorizontal: R.dimensions.h40,
                  borderBottomWidth: index == 0 ? 2 : 0,
                  borderColor: '#ff4b14',
                  marginLeft: index == 0 ? R.dimensions.h40 : 0,
                }}>
                <Text
                  style={[
                    R.styles.normalFont,
                    { borderColor: '#ff4b14', color: index == 0 ? '#ff4b14' : '#777777', fontWeight: 'normal' },
                  ]}>
                  {text}
                </Text>
              </View>
            ))}
          </ScrollView>
        </View>

        <SafeAreaView style={{ flex: 1, width: '100%', paddingTop: R.dimensions.v5 }}>
          <FlatList renderItem={this.ImageItems} data={imageList} />
        </SafeAreaView>
      </View>
    )
  }
}

export default Mainscreen

export const styles = StyleSheet.create({
  headerTextStyle: {
    ...R.styles.normalFont,
    marginLeft: scale(16),
    flex: 1,
    textAlignVertical: 'center',
    fontFamily: R.fonts.NunitoSans_ExtraBold,
    fontSize: 17,
    fontWeight: '900',
    fontStyle: 'normal',
    lineHeight: 20.4,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#212f49',
  },
})

const headertextList = ['Pasta', 'Pizza', 'Risotto', 'Deserts']

const imageList = [
  {
    title: 'Spaghetti alla puttanesca',
    body: 'Spaghetti alla puttanesca is an Italian pasta dish invented in Naples in the mid-20th century. ',
    price: '$19',
    weight: '300 gr.',
    image: require('../../images/food1.jpg'),
    id: '1',
  },
  {
    title: "Penne All'Arrabbiata",
    body:
      "Arrabbiata sauce, or sugo all'arrabbiata in Italian, is a spicy sauce for pasta made from garlic, tomatoes, and dried red chili peppers cooked in olive oil. The sauce originates from the Lazio region, around Rome.",
    price: '$15',
    weight: '250 gr.',
    image: require('../../images/food2.jpg'),
    id: '2',
  },
  {
    title: 'Penne Ragu Al forno',
    body:
      'Pasta al forno is a popular dish in Italy on Sundays or public holidays, especially in the South. This is probably because it takes longer to make than recipes with pasta which has only been boiled, so it is too time consuming for a week day meal.',
    price: '$15',
    weight: '350 gr.',
    image: require('../../images/food3.jpeg'),
    id: '3',
  },
  {
    title: 'Spaghetti Aglio Olio E Peperoncino',
    body: 'Spaghetti alla puttanesca is an Italian pasta dish invented in Naples in the mid-20th century. ',
    price: '$22',
    weight: '300 gr.',
    image: require('../../images/food4.jpeg'),
    id: '4',
  },
  {
    title: 'Spaghetti alla puttanesca',
    body: 'Spaghetti alla puttanesca is an Italian pasta dish invented in Naples in the mid-20th century. ',
    price: '$19',
    weight: '300 gr.',
    image: require('../../images/food5.jpg'),
    id: '5',
  },
  {
    title: 'Spaghetti alla puttanesca',
    body: 'Spaghetti alla puttanesca is an Italian pasta dish invented in Naples in the mid-20th century. ',
    price: '$19',
    weight: '300 gr.',
    image: require('../../images/food6.jpeg'),
    id: '6',
  },
  {
    title: 'Spaghetti alla puttanesca',
    body: 'Spaghetti alla puttanesca is an Italian pasta dish invented in Naples in the mid-20th century. ',
    price: '$19',
    weight: '300 gr.',
    image: require('../../images/food7.jpg'),
    id: '7',
  },
]

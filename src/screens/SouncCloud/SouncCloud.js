import React, { useEffect } from 'react'
import { View, Text, Alert } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'

import R from '../../res/R'

const SouncCloud = (props) => {
  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
      <Text style={R.styles.normalFont}>hello</Text>
    </View>
  )
}

export default observer(SouncCloud)

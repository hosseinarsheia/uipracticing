import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, StatusBar, Image, TouchableOpacity, Animated } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { Transition } from 'react-navigation-fluid-transitions'
import { Icon } from 'react-native-elements'

import R from '../../res/R'

@observer
class MarsPicScreen3 extends Component {
  opacity = new Animated.Value(0)

  componentDidMount = () => {
    Animated.timing(this.opacity, { toValue: 1, duration: 1000, useNativeDriver: true }).start()
  }
  render() {
    const opacity = this.opacity.interpolate({
      inputRange: [0, 0.8, 1],
      outputRange: [0, 0, 1],
    })

    return (
      <View style={{ flex: 1, backgroundColor: 'black' }}>
        <Image source={require('../../images/starts.jpg')} style={R.styles.backgroundImage} />
        <StatusBar hidden />

        <TouchableOpacity
          activeOpacity={1}
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          onPress={() => this.props.navigation.navigate('MarsPicScreen4')}>
          <Transition appear="scale" shared="circle">
            <Image
              source={require('../../images/mars1.png')}
              style={{
                position: 'absolute',
                width: 1000,
                height: 1000,
                position: 'absolute',
                top: -150,
                left: -600,
              }}
            />
          </Transition>

          <Animated.View
            style={{
              position: 'absolute',
              zIndex: 1000,
              left: 130,
              top: 350,
              opacity: opacity,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={[R.styles.normalFont, { color: 'white', fontSize: 13, fontStyle: 'italic' }]}>Delta Base</Text>
            <Transition appear="scale" Disappear="scale" shared="base">
              <Image
                source={require('../../images/baseMars.jpg')}
                style={{ width: 25, height: 25, borderRadius: 500 }}
                // resizeMode="cover"
              />
            </Transition>
          </Animated.View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default MarsPicScreen3

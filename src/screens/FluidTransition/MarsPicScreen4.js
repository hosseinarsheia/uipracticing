import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Button, Image, TouchableOpacity, Animated, StatusBar } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { Transition } from 'react-navigation-fluid-transitions'
import { Icon } from 'react-native-elements'

import R from '../../res/R'

@observer
class MarsPicScreen4 extends Component {
  opacity = new Animated.Value(0)

  componentDidMount = () => {
    Animated.timing(this.opacity, { toValue: 1, duration: 1000, useNativeDriver: true }).start()
  }
  render() {
    const opacity = this.opacity.interpolate({
      inputRange: [0, 0.8, 1],
      outputRange: [0, 0, 1],
    })

    return (
      <View style={{ flex: 1, backgroundColor: 'black' }}>
        <StatusBar hidden />
        <View style={{ flex: 1 }}>
          <Transition appear="right" shared="base">
            <Image
              source={require('../../images/baseMars.jpg')}
              style={{ width: null, height: null, flex: 1 }}
              // resizeMode="cover"
            />
          </Transition>
        </View>
      </View>
    )
  }
}

export default MarsPicScreen4

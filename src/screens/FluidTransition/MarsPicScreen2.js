import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, StatusBar, Image, TouchableOpacity, Animated } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { Transition } from 'react-navigation-fluid-transitions'
import { Icon } from 'react-native-elements'

import R from '../../res/R'

@observer
class MarsPicScreen2 extends Component {
  opacity = new Animated.Value(0)

  componentDidMount = () => {
    Animated.timing(this.opacity, { toValue: 1, duration: 1000, useNativeDriver: true }).start()
  }
  render() {
    const opacity = this.opacity.interpolate({
      inputRange: [0, 0.8, 1],
      outputRange: [0, 0, 1],
    })

    return (
      <View style={{ flex: 1, backgroundColor: 'black' }}>
        <Image source={require('../../images/starts.jpg')} style={R.styles.backgroundImage} />
        <StatusBar hidden />

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('MarsPicScreen3')}
          activeOpacity={1}
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Transition appear="scale" Disappear="scale" shared="circle">
            <Image
              source={require('../../images/mars1.png')}
              style={{ position: 'absolute', width: 400, height: 400, transform: [{ rotate: '60deg' }] }}
            />
          </Transition>

          <Animated.View style={{ position: 'absolute', zIndex: 1000, right: 110, top: 360, opacity: opacity }}>
            <Icon name="map-marker-alt" type="fontisto" color="yellow" size={30} />
          </Animated.View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default MarsPicScreen2

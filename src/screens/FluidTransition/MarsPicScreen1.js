import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Button, Image, TouchableOpacity, StatusBar } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { Transition } from 'react-navigation-fluid-transitions'
// import * as Animatable from 'react-native-animatable'

import R from '../../res/R'
import TextAnimatoorComponent from '../../components/TextAnimatoorComponent'

@observer
class MarsPicScreen1 extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'black', justifyContent: 'center', alignItems: 'center' }}>
        <Image source={require('../../images/starts.jpg')} style={R.styles.backgroundImage} />
        <StatusBar hidden />

        <View style={{ top: 200, position: 'absolute', justifyContent: 'center', alignItems: 'center' }}>
          <TextAnimatoorComponent
            HeaderText="MARS"
            HeaderTextStyle={{ color: 'white', fontSize: 50, fontWeight: 'bold', fontStyle: 'italic' }}
            content="OUR NEW HOME "
            textStyle={{ color: 'white', fontSize: R.fontSizes.fs18 }}
          />
        </View>

        <Transition appear="scale" Disappear="scale" shared="circle">
          <TouchableOpacity
            style={{ position: 'absolute', width: 500, height: 500, bottom: -250 }}
            onPress={() => this.props.navigation.navigate('MarsPicScreen2')}
            activeOpacity={1}>
            <Image source={require('../../images/mars1.png')} style={{ width: 500, height: 500 }} />
          </TouchableOpacity>
        </Transition>
      </View>
    )
  }
}

export default MarsPicScreen1

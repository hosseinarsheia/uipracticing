import React, { useEffect, useRef } from 'react'
import { View, Text, Alert, Button } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import * as d3 from 'd3'
import SVG, { Svg, Rect, Circle, Path, Text as SVGText, Defs, LinearGradient, Stop, ClipPath } from 'react-native-svg'
import * as Animatable from 'react-native-animatable'

import R from '../../res/R'
import MyDropdown from '../../components/MyDropdown'
import ActionBar from '../../components/ActionBar'

// const data = [
//   { width: 50, height: 50, color: 'blue', x: 0, y: 0 },
//   { width: '50', height: '50', color: 'red', x: 0, y: 55 },
//   { width: '50', height: '50', color: 'blue', x: 0, y: 110 },
// ]
// const data = [
//   { name: 'veg soup', orders: 200 },
//   { name: 'veg curry', orders: 600 },
//   { name: 'veg pasta', orders: 300 },
//   { name: 'veg burger', orders: 700 },
//   { name: 'veg surprise', orders: 900 },
// ]

const D3Testing = (props) => {
  const localStore = useLocalStore(() => ({
    data: [
      { name: 'veg soup', orders: 200 },
      { name: 'veg curry', orders: 600 },
      { name: 'veg pasta', orders: 300 },
      { name: 'veg burger', orders: 700 },
      { name: 'veg surprise', orders: 900 },
    ],
  }))
  const x = d3
    .scaleBand()
    .domain(localStore.data.map((item) => item.name))
    .range([0, 200])
    .paddingInner(0.2)
    .paddingOuter(0.2)

  const y = d3
    .scaleLinear()
    .domain([0, d3.max(localStore.data, (d) => d.orders)])
    .range([0, 190])

  const AxisX = d3.axisBottom(x)
  const AxisY = d3.axisLeft(y)
  d3.line

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <View style={{ borderWidth: 1 }}>
        <Svg width="200" height="200">
          {localStore.data.map((value) => (
            <Rect x={x(value.name)} width={x.bandwidth()} height={y(value.orders)} fill="orange" strokeWidth="2" />
          ))}
        </Svg>
      </View>

      <Button
        title="press me"
        onPress={() => {
          localStore.data[0].orders = localStore.data[0].orders + 50
        }}
      />
    </View>
  )
}

export default observer(D3Testing)

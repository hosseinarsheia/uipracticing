import React, { useEffect } from 'react'
import { View, Text, Alert, SafeAreaView, StyleSheet, MaskedViewIOS, ScrollView, Image } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'
import { Icon } from 'react-native-elements'

import R from '../../res/R'

const tabWidth = 100
const tabHeight = 40

const GoogleChromeTab = (props) => {
  return (
    <SafeAreaView style={{ flex: 1, alignItems: 'center', backgroundColor: '#212223' }}>
      <View style={{ marginVertical: 20 }}>
        <View style={{ flexDirection: 'row', borderRadius: 5, overflow: 'hidden' }}>
          <Icon name="eye" type="evilicon" color="white" size={50} containerStyle={styles.iconCainainerStyle} />
          <Icon name="close" type="evilicon" color="white" size={50} containerStyle={styles.iconCainainerStyle} />
          <Icon name="lock" type="evilicon" color="white" size={50} containerStyle={styles.iconCainainerStyle} />
        </View>

        <MaskedViewIOS
          style={StyleSheet.absoluteFill}
          maskElement={<View style={[styles.iconCainainerStyle, { left: 50 }]} />}>
          <View style={{ flexDirection: 'row', borderRadius: 5, overflow: 'hidden' }}>
            <Icon
              name="eye"
              type="evilicon"
              color="grey"
              size={50}
              containerStyle={[styles.iconCainainerStyle, { backgroundColor: 'white' }]}
            />
            <Icon
              name="close"
              type="evilicon"
              color="grey"
              size={50}
              containerStyle={[styles.iconCainainerStyle, { backgroundColor: 'white' }]}
            />
            <Icon
              name="lock"
              type="evilicon"
              color="grey"
              size={50}
              containerStyle={[styles.iconCainainerStyle, { backgroundColor: 'white' }]}
            />
          </View>
        </MaskedViewIOS>

        <ScrollView
          horizontal
          style={StyleSheet.absoluteFill}
          pagingEnabled
          contentContainerStyle={{ flex: 1, width: '100%', backgroundColor: 'red' }}>
          <Image source={require('../../images/food1.jpg')} style={{ width: '100%', height: '100%' }} />
          <Image source={require('../../images/food2.jpg')} style={{ width: '100%', height: '100%' }} />
          <Image source={require('../../images/food3.jpeg')} style={{ width: '100%', height: '100%' }} />
          <Image source={require('../../images/food4.jpeg')} style={{ width: '100%', height: '100%' }} />
          <Image source={require('../../images/food5.jpg')} style={{ width: '100%', height: '100%' }} />
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default observer(GoogleChromeTab)

const styles = StyleSheet.create({
  iconCainainerStyle: {
    backgroundColor: 'grey',
    // paddingHorizontal: 10,
    width: tabWidth,
    height: tabHeight,
  },
})

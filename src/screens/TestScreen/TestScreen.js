// import React, { useEffect, useState, Fragment } from 'react'
// import {
//   View,
//   Text,
//   Alert,
//   Animated,
//   SafeAreaView,
//   StatusBarIOS,
//   StatusBar,
//   Button,
//   Platform,
//   TouchableOpacity,
// } from 'react-native'
// import { observable, action, autorun, computed } from 'mobx'
// import { inject, observer, useLocalStore } from 'mobx-react'
// import { getStatusBarHeight } from 'react-native-status-bar-height'

// import { ScrollView, TapGestureHandler, State } from 'react-native-gesture-handler'
// import * as Animatable from 'react-native-animatable'
// import R from '../../res/R'
// import ActionBar from '../../components/ActionBar'

// const scale = new Animated.Value(1)

// const TestScreen = (props) => {
//   const [mine, mineSet] = useState(false)

//   const changeViewSize = () => {
//     Animated.timing(scale, {
//       toValue: 50,
//       duration: 300,
//       useNativeDriver: true,
//     }).start((finished) => {
//       console.log(finished)
//       if (finished) {
//         props.navigation.navigate('D3Testing')
//         setTimeout(() => {
//           scale.setValue(1)
//         }, 500)
//       }
//     })
//   }

//   return (
//     <Fragment>
//       <View style={{ width: '100%', height: getStatusBarHeight(true), backgroundColor: 'white' }} />
//       <SafeAreaView style={{ flex: 1 }}>
//         <View style={{ flex: 1, overflow: 'hidden' }}>
//           {/*
//             <ActionBar title="صفحه ی اصلی" />
//             <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 0 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} />
//           <View style={{ width: 200, height: 100, backgroundColor: 'blue', marginTop: 10 }} /> */}
//           <TapGestureHandler
//             onHandlerStateChange={({ nativeEvent }) => {
//               if (nativeEvent.oldState === State.ACTIVE) {
//                 changeViewSize()
//               }
//               // console.log(nativeEvent)
//             }}>
//             <Animatable.View
//               ref={(ref) => (this.view = ref)}
//               style={{
//                 position: 'absolute',
//                 width: 50,
//                 height: 50,
//                 borderRadius: 500,
//                 backgroundColor: 'red',
//                 zIndex: 100,
//                 top: 200,
//                 left: 100,
//                 overflow: 'hidden',
//                 transform: [{ scale: scale }],
//               }}></Animatable.View>
//           </TapGestureHandler>
//         </View>
//       </SafeAreaView>
//     </Fragment>
//   )
// }

// export default observer(TestScreen)

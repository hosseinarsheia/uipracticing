import React, { useEffect } from 'react'
import { View, Text, Alert, Animated, Button, Dimensions } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'

import R from '../../res/R'
// import Animated from 'react-native-reanimated'

const animeValue = new Animated.ValueXY()

let viewHeight
let viewWidth
const TestingAnimation = (props) => {
  const startAnimation = () => {
    Animated.sequence([
      Animated.spring(animeValue.y, { toValue: R.dimensions.window.height - viewHeight }),
      Animated.spring(animeValue.x, { toValue: R.dimensions.window.width - 100 }),
    ]).start(() => {
      console.log(animeValue.x)
    })
  }

  const getDimensions = ({ nativeEvent }) => {
    viewHeight = nativeEvent.layout.height
    viewWidth = nativeEvent.layout.width
  }

  return (
    <View style={{ flex: 1 }}>
      <View onLayout={getDimensions}>
        <Animated.View
          style={{ width: 100, height: 100, backgroundColor: 'tomato', transform: animeValue.getTranslateTransform() }}
        />
      </View>

      <View style={{ position: 'absolute', top: 150, left: 100 }}>
        <Button title="press me" onPress={startAnimation} />
      </View>
    </View>
  )
}

export default observer(TestingAnimation)

import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { ViewPropTypes, Text, View, StyleSheet, TouchableHighlight } from 'react-native'

import { Icon } from 'react-native-elements'
import Spinner from 'react-native-spinkit'
import R from '../../res/R'

export default class MyButton extends Component {
  state = {
    buttonWidth: -1,
    buttonHeight: R.dimensions.buttonHeight,
    isMeasured: false,
  }

  measureView = (event) => {
    //alert(this.props.title + ":\n\n" + event.nativeEvent.layout.width)
    // if(event.nativeEvent.layout.width < 10) return;
    if (this.state.isMeasured) return

    let hPadding = 0
    if (this.props.buttonStyle && StyleSheet.flatten(this.props.buttonStyle) && StyleSheet.flatten(this.props.buttonStyle).paddingHorizontal)
      hPadding = StyleSheet.flatten(this.props.buttonStyle).paddingHorizontal

    this.setState({
      buttonWidth: event.nativeEvent.layout.width + hPadding,
      buttonHeight: event.nativeEvent.layout.height,
      isMeasured: true,
    })
  }

  componentDidMount() {
    // if(this.props.buttonStyle && this.props.buttonStyle.width)
    //   this.setState({ buttonWidth: this.props.buttonStyle.width});
    // else
    //   this.setState({ buttonWidth: '100%'});
  }

  /*componentWillReceiveProps(nextProps){
    if(nextProps.loading!==this.props.loading){
      //Perform some operation
      this.setState({isMeasured: false });
      // alert("change")
    }
  }
  

  static getDerivedStateFromProps(nextProps, prevState){
    if(nextProps.loading !== prevState.someValue){
      return { someState: nextProps.someValue};
    }
    else return null;
  }
 
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.someValue!==this.props.someValue){
      //Perform some operation here
      this.setState({someState: someValue});
    }
  }*/

  render() {
    var {
      ButtonComponent,
      loading,
      loadingColor,
      loadingType,
      showContentOnLoading,
      icon,
      iconType,
      iconColor,
      iconSize,
      iconStyle,
      ViewComponent,
      title,
      titleStyle,
      buttonStyle,
      underlayColor,
      linearGradientProps,
      ViewComponentStyle,
      ...otherProps
    } = this.props

    if (!ButtonComponent) ButtonComponent = TouchableHighlight

    if (!ViewComponent) ViewComponent = View

    /*var TouchableComponent =
          Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

    if ( Platform.OS === 'android' ) {
      if (Platform.Version >= 21) {
        otherProps.background = TouchableNativeFeedback.Ripple(
          'ThemeAttrAndroid',
          true
        );
      } else {
        otherProps.background = TouchableNativeFeedback.SelectableBackground();
      }
    }*/

    return (
      <ButtonComponent
        {...otherProps}
        style={StyleSheet.flatten([
          {
            height: R.dimensions.buttonHeight,
            elevation: 0,
            backgroundColor: R.colors.primaryColor,
          },
          buttonStyle,
          {
            paddingLeft: 0,
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            flexDirection: 'row',
          },
        ])}
        underlayColor={underlayColor}
        onLayout={(event) => this.measureView(event)}>
        <ViewComponent
          {...linearGradientProps}
          style={[
            {
              height: R.dimensions.buttonHeight,
              justifyContent: 'center',
            },
            buttonStyle,
            ViewComponentStyle,
            {
              flex: 1,
              // width: '100%', //this.state.buttonWidth,
              height: this.state.buttonHeight,
              backgroundColor: 'transparent',
              alignSelf: 'stretch',
              flexDirection: 'row',
              borderWidth: 0,
              marginTop: 0,
              marginBottom: 0,
              marginLeft: 0,
              marginRight: 0,
              elevation: 0,
              alignItems: 'center',
            },
          ]}>
          {loading ? <Spinner color={loadingColor} type={loadingType} size={this.props.spinSize || this.state.buttonHeight / 2} /> : <View />}

          {!loading || (loading && showContentOnLoading) ? (
            <Fragment>
              {title != null ? (
                <Text
                  numberOfLines={1}
                  style={StyleSheet.flatten([
                    {
                      fontFamily: R.fonts.IRANSansMobile_Bold,
                      fontSize: R.fontSizes.normal,
                      color: R.colors.white,
                      textAlign: 'center',
                    },
                    titleStyle,
                  ])}>
                  {title}
                </Text>
              ) : (
                <View />
              )}

              {icon ? (
                <Icon
                  containerStyle={title && { marginLeft: R.dimensions.h10 }}
                  name={icon}
                  type={iconType}
                  color={iconColor}
                  size={iconSize}
                  iconStyle={iconStyle}
                />
              ) : (
                <View />
              )}
            </Fragment>
          ) : null}
        </ViewComponent>
      </ButtonComponent>
    )
  }
}

MyButton.propTypes = {
  ...ViewPropTypes,
  ButtonComponent: PropTypes.oneOfType([PropTypes.element, PropTypes.any]),
  loading: PropTypes.bool,
  loadingColor: PropTypes.string,
  loadingType: PropTypes.string,
  showContentOnLoading: PropTypes.bool,
  icon: PropTypes.string,
  iconType: PropTypes.string,
  iconColor: PropTypes.string,
  iconSize: PropTypes.number,
  iconStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  ViewComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.element, PropTypes.any]),
  title: PropTypes.string,
  titleStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  buttonStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  underlayColor: PropTypes.string,
  spinSize: PropTypes.number,
}

MyButton.defaultProps = {
  ViewComponent: View,
  // buttonStyle: { height: R.dimensions.buttonHeight },
  underlayColor: R.colors.primaryDarkColor,
  ButtonComponent: TouchableHighlight,
  loadingColor: R.colors.white,
  loadingType: 'Circle',
  showContentOnLoading: false,
  iconType: 'font-awesome',
  iconColor: R.colors.primaryColor,
  iconSize: R.dimensions.buttonIcon,
  spinSize: 0,
}

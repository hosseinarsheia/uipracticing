import React, { Component, Fragment } from 'react'
import { View, Text, Alert, StyleSheet, Button, Animated } from 'react-native'
import { observable, action, autorun, computed } from 'mobx'
import { inject, observer, useLocalStore } from 'mobx-react'

import R from '../../res/R'

@observer
class TextAnimatoorComponent extends Component {
  animatedValue = []

  constructor(props) {
    super(props)
    const textArr = props.content.trim().split(' ')

    textArr.forEach((_, i) => {
      this.animatedValue[i] = new Animated.Value(0)
    })
    this.textArr = textArr
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.animated()
    }, 1300)
  }

  animated = (toValue = 1) => {
    const animations = this.textArr.map((_, i) => {
      return Animated.timing(this.animatedValue[i], {
        toValue,
        duration: 500,
        useNativeDriver: true,
      })
    })

    Animated.stagger(120, animations).start()
  }

  render() {
    const { textStyle, style, content, HeaderText, HeaderTextStyle } = this.props
    return (
      <Fragment>
        <Animated.Text
          style={[
            HeaderTextStyle,
            {
              opacity: this.animatedValue[0],
              // transform: [{ translateY: Animated.multiply(this.animatedValue[index], -50) }],
            },
          ]}>
          {HeaderText}
        </Animated.Text>
        <View style={[styles.textContainer, style]}>
          {this.textArr.map((text, index) => (
            <Fragment>
              <Animated.Text
                key={index}
                style={[
                  textStyle,
                  {
                    opacity: this.animatedValue[index],
                    // transform: [{ translateY: Animated.multiply(this.animatedValue[index], -50) }],
                  },
                ]}>
                {text}{' '}
              </Animated.Text>
            </Fragment>
          ))}
        </View>
      </Fragment>
    )
  }
}

export default TextAnimatoorComponent

export const styles = StyleSheet.create({
  textContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
})

import React from 'react'
import { View } from 'react-native'
import Spinner from 'react-native-spinkit'

import R from '../../res/R'

const MyLoading = (props) => {
  return (
    <View style={[{ width: '100%', alignItems: 'center', justifyContent: 'center', padding: R.dimensions.b5 }, props.containerStyle]}>
      <Spinner
        isVisible={props.visible}
        color={props.loadingColor || R.colors.primaryColor}
        type={props.loadingType || 'WanderingCubes'}
        size={props.spinSize || R.dimensions.loadingSize}
      />
    </View>
  )
}
export default MyLoading

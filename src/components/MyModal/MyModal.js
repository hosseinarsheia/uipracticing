import React, { Component, Fragment } from 'react'
import { View, TouchableWithoutFeedback, Modal, StyleSheet } from 'react-native'
import { observer } from 'mobx-react'
import R from '../../res/R'

@observer
export default class MyModal extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Modal style={{ width: '100%', height: '100%' }} visible={this.props.visible} transparent={true} onRequestClose={this.props.onRequestClose}>
        <TouchableWithoutFeedback onPress={this.props.onTouchOutside}>
          <View style={styles.pageWrapper}>
            <TouchableWithoutFeedback onPress={() => {}}>
              <Fragment>{this.props.children}</Fragment>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

export const styles = StyleSheet.create({
  pageWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: R.colors.modalBack,
  },
})

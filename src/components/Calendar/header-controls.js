/**
 * Persian Calendar Picker Component
 *
 * Copyright 2016 Reza (github.com/rghorbani)
 * Licensed under the terms of the MIT license. See LICENSE file in the project root for terms.
 */

'use strict'
import React, { Component } from 'react'
// const React = require('react');
const PropTypes = require('prop-types')
const { Text, View, Button } = require('react-native')

const Utils = require('./utils')
const Controls = require('./controls')

import { observable } from 'mobx'
import { observer } from 'mobx-react'
import MyUtils from '../../utils/Utils'
import Picker from 'react-native-wheel-picker'
const PickerItem = Picker.Item

@observer
class HeaderControls extends Component {
  @observable selectedYearIndex = 50
  @observable selectedMonthIndex = this.props.currentMonth
  @observable yearList = []
  @observable isInit = false

  constructor(props) {
    super(props)

    this.yearList = []
    for (let y = -50; y < 51; y++) {
      this.yearList.push(`${MyUtils.toPersianNumber(props.currentYear + y)}`)
    }

    if (this.changeYearTimeout) clearTimeout(this.changeYearTimeout)
    if (this.changeMonthTimeout) clearTimeout(this.changeMonthTimeout)
  }

  componentDidMount = () => {
    this.yearList = [...this.yearList]
    this.selectedYearIndex = 50
    this.selectedMonthIndex = this.props.currentMonth
    this.isInit = true
  }

  changeYear = () => {
    if (!this.isInit) return
    const { onYearChange } = this.props

    if (onYearChange && typeof onYearChange == 'function') {
      if (this.changeYearTimeout) clearTimeout(this.changeYearTimeout)
      this.changeYearTimeout = setTimeout(() => {
        onYearChange(MyUtils.toEnglishNumber(this.yearList[this.selectedYearIndex]))
      }, 20)
    }
  }

  changeMonth = () => {
    if (!this.isInit) return
    const { onMonthChange } = this.props

    if (onMonthChange && typeof onMonthChange == 'function') {
      if (this.changeMonthTimeout) clearTimeout(this.changeMonthTimeout)
      this.changeMonthTimeout = setTimeout(() => {
        onMonthChange(this.selectedMonthIndex)
      }, 20)
    }
  }

  render() {
    const {
      styles,
      currentMonth,
      currentYear,
      onMonthChange,
      onYearChange,
      onPressNext,
      onPressPrevious,
      months,
      previousTitle,
      nextTitle,
      textStyle,
    } = this.props
    const MONTHS = months ? months : Utils.MONTHS // English Month Array
    // getMonth() call below will return the month number, we will use it as the
    // index for month array in english
    const previous = previousTitle ? previousTitle : 'قبلی'
    const next = nextTitle ? nextTitle : 'بعدی'
    const month = MONTHS[currentMonth]
    const year = currentYear

    return (
      <View style={styles.headerWrapper}>
        <View style={{ width: 100, height: 100, overflow: 'hidden' }}>
          <Picker
            selectedValue={this.selectedMonthIndex}
            itemSpace={15}
            style={{ width: 100, height: 120, marginTop: -10 }}
            itemStyle={{ color: 'white', fontSize: 20 }}
            onValueChange={index => {
              this.selectedMonthIndex = index
              this.changeMonth()
            }}>
            {MONTHS.map((value, i) => (
              <PickerItem label={value} value={i} key={'month' + value} />
            ))}
          </Picker>
        </View>

        <View style={{ width: 100, height: 100, overflow: 'hidden' }}>
          <Picker
            selectedValue={this.selectedYearIndex}
            itemSpace={25}
            style={{ width: 100, height: 120, marginTop: -10 }}
            itemStyle={{ color: 'white', fontSize: 20 }}
            onValueChange={index => {
              this.selectedYearIndex = index
              this.changeYear()
            }}>
            {this.yearList.map((value, i) => (
              <PickerItem label={value} value={i} key={'year' + value} />
            ))}
          </Picker>
        </View>

        {/* <Controls
          label={previous}
          onPressControl={onPressPrevious}
          styles={[styles.monthSelector, styles.prev]}
          textStyles={textStyle}
        />
        <View>
          <Text style={[styles.monthLabel, textStyle]}>
            {month} {year}
          </Text>
        </View>
        <Controls
          label={next}
          onPressControl={onPressNext}
          styles={[styles.monthSelector, styles.next]}
          textStyles={textStyle}
        /> */}
      </View>
    )
  }
}

HeaderControls.propTypes = {
  currentMonth: PropTypes.number,
  currentYear: PropTypes.number,
  onYearChange: PropTypes.func,
  onPressNext: PropTypes.func,
  onPressPrevious: PropTypes.func,
}

// module.exports = HeaderControls;
export default HeaderControls

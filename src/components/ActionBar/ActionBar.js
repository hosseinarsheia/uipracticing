import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ViewPropTypes, Text, View, StatusBar } from 'react-native'
import { withNavigation } from 'react-navigation'
import Button from '../MyButton'
import R from '../../res/R'

class ActionBar extends Component {
  constructor(props) {
    super(props)

    this.setProps()
  }

  setProps() {
    StatusBar.setHidden(this.props.hideStatusBar)
    if (this.props.transparentStatusBar) {
      StatusBar.setTranslucent(true)
      StatusBar.setBackgroundColor('transparent', true)
    } else {
      StatusBar.setTranslucent(false)
      StatusBar.setBackgroundColor(this.props.statusBarColor, true)
    }

    StatusBar.setBarStyle(this.props.statusBarStyle)
  }

  render() {
    /*var showBackButton = this.props.commandType === 'Push' ||
                          this.props.commandType === 'ShowModal';*/

    const rightButton = this.props.showBackButton
      ? {
          icon: 'arrow-right', //angle-right
          iconType: 'font-awesome',
          iconSize: dActionBarSize * 0.35,
          iconStyle: { fontWeight: '100' },
          onPress: () => {
            if (this.props.backButtonHandler && typeof this.props.backButtonHandler === 'function') this.props.backButtonHandler()
            else this.props.navigation.goBack()
          },
        }
      : this.props.rightButton

    return (
      <View style={[this.props.containerStyle, { width: '100%', zIndex: 1000 }]}>
        <StatusBar
          translucent={this.props.transparentStatusBar}
          backgroundColor={this.props.transparentStatusBar ? 'transparent' : this.props.statusBarColor}
          barStyle={this.props.statusBarStyle}
        />

        {!this.props.hideActionBar && (
          <View
            style={[
              {
                height: dActionBarSize,
                backgroundColor: this.props.backgroundColor,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                elevation: 0,
                alignItems: 'center',
              },
              this.props.style,
            ]}>
            {this.props.leftButton ? (
              <Button
                buttonStyle={[
                  { width: dActionBarSize, height: dActionBarSize, elevation: 0, borderRadius: dActionBarSize / 2 },
                  this.props.leftButton.buttonStyle,
                ]}
                ViewComponent={this.props.leftButton.ViewComponent}
                ViewComponentStyle={this.props.leftButton.ViewComponentStyle}
                loading={this.props.leftButton.loading}
                loadingColor={this.props.leftButton.loadingColor}
                loadingType={this.props.leftButton.loadingType}
                title={this.props.leftButton.title || ''}
                titleStyle={this.props.leftButton.titleStyle}
                icon={this.props.leftButton.icon}
                iconType={this.props.leftButton.iconType}
                iconColor={this.props.leftButton.iconColor || R.colors.white}
                iconSize={this.props.leftButton.iconSize || dActionBarSize * 0.5}
                iconStyle={this.props.leftButton.iconStyle}
                underlayColor={R.colors.white + '33'}
                onPress={() => {
                  if (this.props.leftButton.onPress) this.props.leftButton.onPress()
                }}
              />
            ) : rightButton ? (
              <View style={{ width: dActionBarSize, height: dActionBarSize }} />
            ) : null}

            {this.props.children ? (
              this.props.children
            ) : (
              <Text
                style={[
                  {
                    flex: 1,
                    textAlign: 'center',
                    color: 'white',
                    fontSize: R.fontSizes.fs19,
                    fontFamily: R.fonts.IRANSansMobile,
                  },
                  this.props.titleStyle,
                ]}>
                {this.props.title}
              </Text>
            )}

            {rightButton ? (
              <Button
                buttonStyle={[
                  { width: dActionBarSize, height: dActionBarSize, elevation: 0, borderRadius: dActionBarSize / 2 },
                  rightButton.buttonStyle,
                ]}
                loading={rightButton.loading}
                loadingColor={rightButton.loadingColor}
                loadingType={rightButton.loadingType}
                title={rightButton.title || ''}
                titleStyle={rightButton.titleStyle}
                icon={rightButton.icon}
                iconType={rightButton.iconType}
                iconColor={rightButton.iconColor || R.colors.white}
                iconSize={rightButton.iconSize || dActionBarSize * 0.5}
                iconStyle={rightButton.iconStyle}
                underlayColor={R.colors.white + '33'}
                onPress={() => {
                  if (rightButton.onPress) rightButton.onPress()
                }}
              />
            ) : this.props.leftButton ? (
              <View style={{ width: dActionBarSize, height: dActionBarSize }} />
            ) : null}
          </View>
        )}
      </View>
    )
  }
}

ActionBar.propTypes = {
  ...ViewPropTypes,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  hideStatusBar: PropTypes.bool,
  transparentStatusBar: PropTypes.bool,
  hideActionBar: PropTypes.bool,
  showBackButton: PropTypes.bool,
  backButtonHandler: PropTypes.func,
  statusBarColor: PropTypes.string,
  statusBarStyle: PropTypes.string,
  backgroundColor: PropTypes.string,
  title: PropTypes.string,
  titleStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
}

ActionBar.defaultProps = {
  hideStatusBar: false,
  transparentStatusBar: false,
  hideActionBar: false,
  showBackButton: true,
  statusBarColor: R.colors.primaryDarkColor,
  statusBarStyle: 'light-content',
  backgroundColor: R.colors.primaryColor,
  title: '',
}

const dActionBarSize = R.dimensions.actionBarSize

export default withNavigation(ActionBar)

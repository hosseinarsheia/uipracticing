import React, { Component } from 'react'
import { Element, View } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Input } from 'react-native-elements'
import R from '../../res/R'
import { styles } from './styles'

class MyInput extends Component {
  render() {
    var {
      refInput,
      icon,
      iconColor,
      iconSize,
      placeholderTextColor,
      containerStyle,
      inputContainerStyle,
      inputStyle,
      labelStyle,
      errorStyle,
      ...otherProps
    } = this.props

    return (
      <Input
        {...otherProps}
        ref={refInput}
        containerStyle={[styles.container, containerStyle]}
        inputContainerStyle={[styles.inputContainer, inputContainerStyle]}
        inputStyle={[styles.inputStyle, { ...inputStyle }]}
        labelStyle={[styles.label, labelStyle]}
        rightIcon={icon && <Icon name={icon} color={iconColor} size={iconSize} />}
        autoFocus={false}
        autoCapitalize="none"
        keyboardAppearance="light"
        errorStyle={[styles.error, errorStyle]}
        renderErrorMessage={false}
        autoCorrect={false}
        blurOnSubmit={false}
        placeholderTextColor={placeholderTextColor}
      />
    )
  }
}

MyInput.propTypes = {
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  inputContainerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  inputStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  labelStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  errorStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  refInput: PropTypes.instanceOf(Element),
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  iconSize: PropTypes.number,
  placeholderTextColor: PropTypes.string,
}

MyInput.defaultProps = {
  iconColor: R.colors.primaryColor,
  iconSize: R.dimensions.inputIcon,
  placeholderTextColor: R.colors.textColor,
}

export default MyInput

import { StyleSheet } from 'react-native'
import R from '../../res/R'

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: R.dimensions.myInputHeight,
    paddingLeft: 0,
    paddingRight: 0,
    flex: 0.6,
  },
  inputContainer: {
    width: '100%',
    borderRadius: R.dimensions.myInputHeight / 10,
    // borderWidth: 1,
    borderColor: R.colors.borderColor,
    backgroundColor: R.colors.myInputBack,
    justifyContent: 'center',
  },
  inputStyle: {
    minHeight: R.dimensions.myInputHeight,
    // backgroundColor: 'yellow',
    flex: 1,
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.normal,
    color: R.colors.black,
    paddingHorizontal: R.dimensions.h5,
    textAlign: 'center',
    textAlignVertical: 'center',
    padding: 0,
  },
  label: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.normal,
    textAlign: 'center',
    marginBottom: R.dimensions.v5,
    fontWeight: '400',
  },
  error: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.fs10,
    color: R.colors.error,
    textAlign: 'center',
    margin: 0,
    includeFontPadding: false,
  },
})

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, TouchableWithoutFeedback, TouchableOpacity, Modal, StyleSheet, Text } from 'react-native'
import R from '../../res/R'
import PersianCalender from '../Calendar'
import jMoment from 'moment-jalaali'
import { Icon } from 'react-native-elements'

import { observable } from 'mobx'
import { observer } from 'mobx-react'

@observer
class MyCalendar extends Component {
  @observable todayBackgroundColor = R.colors.primaryColor
  @observable todayTextColor = 'white'
  @observable isVisible = false

  //calender Component itself
  calendar = () => {
    const { todayBackgroundColor, selectedDayTextColor, onDateChange, maxDate, minDate } = this.props
    return (
      <Modal visible={this.isVisible} transparent={true} style={styles.modalStyle} onRequestClose={() => (this.isVisible = false)}>
        <TouchableWithoutFeedback
          onPress={
            () => (this.isVisible = false) // hide modal when touch on background of modal
          }>
          <View style={styles.calenderWrapper}>
            {/* prevent to close calender when touch on calender itSelf background */}
            <TouchableWithoutFeedback onPress={() => {}}>
              <View style={{ backgroundColor: 'white', borderRadius: R.dimensions.borderRadius }}>
                <PersianCalender
                  isRTL={true}
                  todayBackgroundColor={this.todayBackgroundColor}
                  todayTextStyle={{ color: this.todayTextColor }}
                  initialDate={this.props.dateValue ? jMoment(this.props.dateValue, 'jYYYY / jMM / jDD').utc(true) : new Date()}
                  // give calender's date value to onDateChange function
                  onDateChange={(date) => {
                    onDateChange(date)
                    this.todayBackgroundColor = 'white'
                    this.todayTextColor = 'black'
                    this.isVisible = false
                  }}
                  minDate={minDate} // minimum date that calender can pick
                  maxDate={maxDate} // maximum date that calender can pick
                />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
  render() {
    const { dateValue, containerStyle, labelStyle, label, clearDate, clearDateIcon } = this.props
    return (
      <View style={[styles.rowWrapper, containerStyle]}>
        {this.calendar()}

        <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'flex-end' }}>
          <Icon
            name="calendar"
            type="font-awesome"
            color={R.colors.primaryColor}
            size={30}
            containerStyle={{ paddingRight: R.dimensions.h5 }}
            onPress={() => (this.isVisible = true)}
          />
          <TouchableOpacity style={{ flex: 1 }} onPress={() => (this.isVisible = true)} hitSlop={{ left: 10 }}>
            <Text style={styles.dateTexts}>{dateValue}</Text>
          </TouchableOpacity>

          {clearDateIcon && dateValue ? (
            <TouchableWithoutFeedback
              hitSlop={{ right: 30 }} // can touch X icon , right side to clear date
              onPress={
                () => clearDate() // clearDate function must clear Date object that obtain from onDateChange function
              }>
              <Icon name="close" type="font-awesome" color={R.colors.cancelRequest} size={20} containerStyle={{ paddingLeft: R.dimensions.h10 }} />
            </TouchableWithoutFeedback>
          ) : null}
        </View>

        <Text style={[styles.dateLableTexts, labelStyle]}>{label}</Text>
      </View>
    )
  }
}

export default MyCalendar

MyCalendar.propTypes = {
  todayBackgroundColor: PropTypes.string,
  selectedDayTextColor: PropTypes.string,
  onDateChange: PropTypes.func, // pass date value to  onDateChange function
  containerStyle: PropTypes.object,
  label: PropTypes.string, // calender text label
  labelStyle: PropTypes.object,
  name: PropTypes.string,
  dateValue: PropTypes.string,
  clearDate: PropTypes.func,
  clearDateIcon: PropTypes.bool,
  type: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string,
}

MyCalendar.defaultProps = {
  minSelectedDate: null,
  todayBackgroundColor: R.colors.primaryColor,
  selectedDayTextColor: 'white',
  clearDateIcon: false,
  name: 'calendar',
  type: 'font-awesome',
  size: 30,
  color: R.colors.primaryColor,
}

const styles = StyleSheet.create({
  modalStyle: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  calenderWrapper: {
    flex: 1,
    backgroundColor: R.colors.modalBack,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    paddingHorizontal: R.dimensions.componentsHorizontalPadding,
  },
  dateTexts: {
    ...R.styles.normalFont,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: R.colors.borderColor,
    textAlign: 'center',
    alignSelf: 'stretch',
  },
  dateLableTexts: {
    flex: 0.4,
    ...R.styles.normalFont,
    textAlign: 'right',
  },
})

import React, { Component } from 'react'
import { View, Text, Modal, TouchableWithoutFeedback, TouchableHighlight } from 'react-native'
import R from '../../res/R'
import PropTypes from 'prop-types'
import { observable } from 'mobx'
import { observer } from 'mobx-react'

@observer
class MyAlert extends Component {
  @observable visible = false
  @observable message = ''
  @observable buttons = null

  showAlert = (message, buttons = null, onDismiss = null) => {
    this.message = message
    this.buttons = buttons
    this.onDismiss = onDismiss
    this.visible = true
  }

  onRequestClose = () => {
    this.visible = false
    if (this.onDismiss && typeof this.onDismiss == 'function') this.onDismiss()
  }

  renderButtons() {
    if (this.buttons && this.buttons.length > 0) {
      return this.buttons.map((b, i) => {
        return (
          <TouchableHighlight
            key={`${Math.random()}`}
            style={{
              flex: 1,
              alignItems: 'stretch',
              paddingVertical: R.dimensions.v10,
              borderBottomEndRadius: i == 0 ? R.numbers.borderRadius : 0,
              borderBottomStartRadius: i == this.buttons.length - 1 ? R.numbers.borderRadius : 0,
            }}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => {
              if (b.onPress && typeof b.onPress == 'function') b.onPress()

              this.visible = false
            }}>
            <Text style={[R.styles.normalFont, { textAlign: 'center', color: R.colors.primaryColor }]}>{b.text}</Text>
          </TouchableHighlight>
        )
      })
    } else {
      return (
        <TouchableHighlight
          key={`${Math.random()}`}
          style={{
            flex: 1,
            alignItems: 'stretch',
            paddingVertical: R.dimensions.v10,
            borderBottomEndRadius: R.numbers.borderRadius,
            borderBottomStartRadius: R.numbers.borderRadius,
          }}
          underlayColor={R.colors.buttonGrayUnderlay}
          onPress={() => {
            this.onRequestClose()
          }}>
          <Text style={[R.styles.normalFont, { textAlign: 'center', color: R.colors.primaryColor }]}>{R.strings.ok}</Text>
        </TouchableHighlight>
      )
    }
  }

  render() {
    const { animationType, okButtonHandler } = this.props

    return (
      <Modal animationType={animationType} transparent={true} visible={this.visible} onRequestClose={this.onRequestClose}>
        <TouchableWithoutFeedback onPress={this.onRequestClose}>
          <View style={{ flex: 1, backgroundColor: R.colors.modalBack, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableWithoutFeedback onPress={() => {}}>
              <View
                style={{
                  minWidth: 200,
                  backgroundColor: 'white',
                  borderRadius: R.numbers.borderRadius,
                  marginHorizontal: R.dimensions.h20,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    padding: R.dimensions.b10,
                  }}>
                  <Text style={R.styles.normalFont}> {this.message} </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row-reverse',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  {this.renderButtons()}
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

export default MyAlert

// onRequestClose props should make visible props false

MyAlert.propTypes = {
  animationType: PropTypes.string,
}

MyAlert.defaultProps = {
  animationType: 'fade',
}

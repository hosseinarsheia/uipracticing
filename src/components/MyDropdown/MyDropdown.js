// import React, { Component } from 'react'
// import { Dropdown } from 'react-native-material-dropdown'
// import PropTypes from 'prop-types'
// import { View, Text } from 'react-native'

// import R from '../../res/R'

// class MyDropdown extends Component {
//   render() {
//     const {
//       data,
//       itemTextStyle,
//       fontSize,
//       animationDuration,
//       style,
//       dropdownOffset,
//       rippleInsets,
//       onChangeText,
//       pickerStyle,
//       itemCount,
//       containerStyle,
//       label,
//       errorMessage,
//       wrapperStyle,
//       labelStyle,
//       ...otherProps
//     } = this.props

//     return (
//       <View style={[{ flexDirection: 'row', width: '100%', paddingHorizontal: R.dimensions.componentsHorizontalPadding }, wrapperStyle]}>
//         <Dropdown
//           data={data}
//           itemTextStyle={itemTextStyle}
//           fontSize={fontSize}
//           animationDuration={animationDuration}
//           style={[style, { height: R.fontSizes.normal * 1.8, textAlign: 'center', overflow: 'hidden' }]}
//           containerStyle={[{ flex: 0.6, height: errorMessage ? null : R.fontSizes.normal * 2, overflow: 'hidden' }, containerStyle]}
//           dropdownOffset={dropdownOffset}
//           rippleInsets={rippleInsets}
//           onChangeText={onChangeText}
//           pickerStyle={pickerStyle}
//           itemCount={itemCount}
//           error={errorMessage}
//           {...otherProps}
//         />
//         <Text style={[R.styles.normalFont, { flex: 0.4, paddingTop: R.dimensions.v2, backgroundColor: 'red' }, labelStyle]}>{label}</Text>
//       </View>
//     )
//   }
// }

// export default MyDropdown

// MyDropdown.propTypes = {
//   fontSize: PropTypes.number,
//   animationDuration: PropTypes.number,
//   style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
//   dropdownOffset: PropTypes.object,
//   rippleInsets: PropTypes.object,
//   onChangeText: PropTypes.func,
//   pickerStyle: PropTypes.object,
//   itemCount: PropTypes.number,
//   itemTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
//   titleTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]), // error masseage style
//   label: PropTypes.string,
//   errorMessage: PropTypes.string,
//   wrapperStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
//   labelStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
// }

// MyDropdown.defaultProps = {
//   fontSize: R.fontSizes.normal,
//   animationDuration: R.numbers.animationDuration,
//   style: R.styles.normalFont,
//   itemTextStyle: [R.styles.normalFont, { textAlign: 'center' }],
//   dropdownOffset: { top: 0, right: 0 },
//   rippleInsets: { top: 0, bottom: -8 },
//   pickerStyle: { width: R.dimensions.pickerWidth },
//   itemCount: 8,
//   titleTextStyle: {
//     fontFamily: R.fonts.IRANSansMobile,
//     fontSize: R.fontSizes.small,
//     color: R.colors.error,
//     textAlign: 'center',
//     marginTop: 0,
//   },
// }

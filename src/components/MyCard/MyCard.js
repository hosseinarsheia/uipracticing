import React, { Component } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import PropTypes from 'prop-types'

import R from '../../res/R'

class MyCard extends Component {
  render() {
    const {
      headerText,
      coloredRowStyle,
      children,
      outerPanelStyle,
      headerTextPosition,
      SecondHeaderText,
      firstRow,
      panelStyle,
      headerTextStyle,
    } = this.props
    return (
      <View style={[styles.outerPanelWrapper, { ...outerPanelStyle }]}>
        <View style={[styles.panel, { ...panelStyle }]}>
          {firstRow ? (
            <View style={[styles.coloredRow, { ...coloredRowStyle }]}>
              <Text style={[R.styles.normalFont, { color: 'white', textAlign: headerTextPosition, flex: 1 }, headerTextStyle]}>
                {headerText}
                <Text style={R.styles.smallFont}> {SecondHeaderText}</Text>
              </Text>
            </View>
          ) : null}

          {children}
        </View>
      </View>
    )
  }
}

export default MyCard

export const styles = StyleSheet.create({
  outerPanelWrapper: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    marginBottom: R.dimensions.v10,
    marginTop: 2,
  },
  panel: {
    flex: 1,
    marginHorizontal: R.dimensions.h5,
    borderRadius: R.numbers.borderRadius,
    elevation: R.numbers.elevation,
    backgroundColor: 'white',
  },
  coloredRow: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: R.colors.primaryColor,
    borderTopLeftRadius: R.numbers.borderRadius,
    borderTopRightRadius: R.numbers.borderRadius,
    paddingVertical: R.dimensions.v10,
    paddingHorizontal: R.dimensions.h5,
  },
})

MyCard.propTypes = {
  firstRow: PropTypes.bool,
  headerText: PropTypes.string.isRequired,
  SecondHeaderText: PropTypes.string,
  coloredRowStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  outerPanelStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  panelStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  headerTextPosition: PropTypes.string,
  headerTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
}

MyCard.defaultProps = {
  firstRow: true,
  headerTextPosition: 'right',
}

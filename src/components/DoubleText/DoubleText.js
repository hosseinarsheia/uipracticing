import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import R from '../../res/R'

class DoubleText extends Component {
  render() {
    const { containerStyle, label, labelTextStyle, secondText, secondTextStyle, bottomBorder } = this.props
    return (
      <View style={[styles.container, { borderBottomWidth: bottomBorder ? 1 : 0, ...containerStyle }]}>
        <Text style={[R.styles.normalFont, { ...labelTextStyle }]}>
          {label}
          <Text style={[R.styles.smallFont, { color: 'black', ...secondTextStyle }]}> {secondText} </Text>
        </Text>
      </View>
    )
  }
}

export default DoubleText

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '100%',
    alignItems: 'center',
    paddingVertical: R.dimensions.v10,
    paddingHorizontal: R.dimensions.componentsHorizontalPadding,
    borderColor: R.colors.borderColor,
  },
})

DoubleText.propTypes = {
  containerStyle: PropTypes.object,
  label: PropTypes.string.isRequired,
  labelTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  secondText: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  secondTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  bottomBorder: PropTypes.bool,
}

DoubleText.defaultProps = {
  bottomBorder: true,
}

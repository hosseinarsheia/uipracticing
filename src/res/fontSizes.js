import { moderateScale, verticalScale } from 'react-native-size-matters'

const fontSizes = {
  xxSmall: moderateScale(9),
  xSmall: moderateScale(11),
  small: moderateScale(13),
  normal: moderateScale(15),
  medium: moderateScale(17),
  big: moderateScale(18),
  xBig: moderateScale(20),
  xxBig: moderateScale(25),
  xxxBig: moderateScale(30),
  title: moderateScale(19),
  mine: moderateScale(12),

  fs6: moderateScale(6),
  fs7: moderateScale(7),
  fs8: moderateScale(8),
  fs9: moderateScale(9),
  fs10: moderateScale(10),
  fs11: moderateScale(11),
  fs12: moderateScale(12),
  fs13: moderateScale(13),
  fs14: moderateScale(14),
  fs15: moderateScale(15),
  fs16: moderateScale(16),
  fs17: moderateScale(17),
  fs18: moderateScale(18),
  fs19: moderateScale(19),
  fs20: moderateScale(20),
  fs25: moderateScale(25),
  fs30: moderateScale(30),

  lineHeight: {
    big: verticalScale(30),
  },

  fontWeight: {
    normal: '200',
  },
}

export default fontSizes

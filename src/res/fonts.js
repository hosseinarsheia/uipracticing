const fonts = {
  IRANSansMobile: 'IRANSansWebFaNum',
  IRANSansMobile_Bold: 'IRANSansWebFaNum-Bold',
  IRANSansMobile_Light: 'IRANSansWebFaNum-Light',
  NunitoSans_ExtraBold: 'NunitoSans-ExtraBold',
  NunitoSans_Regular: 'NunitoSans-Regular',
}
export default fonts

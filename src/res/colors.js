const colors = {
  /******************* Components Colors *******************/
  primaryColor: '#0C8BB0',
  primaryDarkColor: '#1E4C73',
  primaryVDarkColor: '#0e3759',
  borderColor: '#cdd1d4',
  green: '#53A93F',
  cancelRequest: '#D73D32',
  error: 'red',
  textColor: 'grey',
  textLink: '#57B5E3',
  placeholderTextColor: '#BDBDBD',
  errorBoxGreen: '#53A93F',
  errorBoxRed: '#D73D32',
  buttonGrayUnderlay: '#cccccc88',
  text_gray: '#cacaca',
  text_label: '#999999',
  text_gray_dark: '#575757',
  white: '#ffffff',
  black: '#000',
  myInputBack: '#ffffff88',
  modalBack: '#000000a5',

  /******************* *******************/
}

export default colors

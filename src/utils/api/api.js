import axios from 'axios'
import aeCodes from './apiErrorCodes'
import Utils from '../Utils'
import { Alert } from 'react-native'

export default class Newapi {
  static instance = null
  static getInstance() {
    if (this.instance == null) {
      this.instance = new Newapi()
    }

    return this.instance
  }

  constructor() {
    axios.interceptors.response.use(
      function (response) {
        DebugLog('========= RESPONSE =========')
        DebugLog(JSON.stringify(response))
        DebugLog('============================')
        // Alert.alert('response', JSON.stringify(response));

        if (response == null || response.data == null) return Promise.reject(aeCodes.WS_ERROR)

        let responseJson = response.data

        if (responseJson.ResultCode != 0 || responseJson.ObjList == null)
          return Promise.reject({
            code: responseJson.ResultCode,
            message: responseJson.ResultMessage,
          })
        else if (responseJson.ObjList != null && responseJson.ResultCode == 0) return responseJson
        // else if (responseJson.code != null) return responseJson.data;
        else return Promise.reject(aeCodes.WS_ERROR)
      },
      function (error) {
        DebugLog('========= ERROR =========')
        DebugLog(JSON.stringify(error))
        DebugLog('============================')

        if (axios.isCancel(error)) {
          return Promise.reject(aeCodes.WS_CANCEL)
        } else return Promise.reject(aeCodes.WS_CONNECT_ERROR)
      },
    )
  }

  wsCall(url, data = {}, baseURL = 'https://pub-cix.ntsw.ir/Services/NTSW_UI', onUploadProgress = null) {
    const CancelToken = axios.CancelToken
    let cancel

    var config = {
      method: 'POST',
      baseURL: baseURL,
      url: url,
      headers: {
        'Content-Type': 'application/json',
        Authorization: Utils.decoding('Bbulg%g^=9dcVgflk^Gwnfirqov{ivef'),
      },

      timeout: 20000,
      data: {
        ...data,
      },
      cancelToken: new CancelToken(function executor(c) {
        cancel = c
      }),
      onUploadProgress: progressEvent => {
        if (onUploadProgress) {
          onUploadProgress(Math.round((progressEvent.loaded * 100) / progressEvent.total))
        }
      },
    }

    return {
      call: async () => {
        DebugLog(`+++++++++ ${url} +++++++++`)
        DebugLog(`+++++++++ ${baseURL} +++++++++`)
        DebugLog(JSON.stringify(data))
        DebugLog('++++++++++++++++++++++++++')

        return axios(config)
      },
      cancel,
    }
  }
}

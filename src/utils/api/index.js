import Newapi from './api'

import aeCodes from './apiErrorCodes'
// import  Storage from '../Storage';
// import { Platform } from 'react-native';
// import DeviceInfo from 'react-native-device-info';
import Utils from '../Utils'

const ApiCall = Newapi.getInstance()

const CHAT_BASE_URL = 'https://pub-cix.ntsw.ir/Services/PubMessageApp'
// const CHAT_BASE_URL = 'http://185.173.168.27/NWA/Api';

export function isCanceled(err) {
  return err.isCanceled
}

export function isWSError(err) {
  switch (err.code) {
    case aeCodes.WS_CONNECT_ERROR.code:
    case aeCodes.WS_ERROR.code:
      return true
    default:
      return false
  }
}

export function getAnnouncement() {
  return ApiCall.wsCall('/Announcement/GetAllAnnouncement', {
    password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
    StartPage: 0,
    PageSize: 1000,
    NewsGroupId: null,
  })
}
export function getNews() {
  return ApiCall.wsCall('News/GetAllNews', {
    password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
    StartPage: 0,
    PageSize: 1000,
    NewsGroupId: null,
    Status: null,
  })
}

export function getِDocumentList(fileGroupId = 0) {
  return ApiCall.wsCall('/Files/GetAllFiles', {
    password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
    StartPage: 0,
    PageSize: 1000,
    FileGroupId: fileGroupId,
  })
}

export function downloadFile(FielId) {
  return ApiCall.wsCall('/Files/GetFileById', {
    password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
    FielId: FielId,
  })
}

/***************************** CHAT API'S *****************************/
export function getMessageList(nationalCode, StartPage, PageSize, isPrivate = null, isSeen = null, searchPhrase) {
  return ApiCall.wsCall(
    '/GetMessageList/',
    {
      nationalCode: nationalCode,
      isPrivate: isPrivate,
      isSeen: isSeen,
      StartPage: StartPage,
      PageSize: PageSize,
      searchPhrase: searchPhrase,
      password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
    },
    CHAT_BASE_URL,
  )
}

// تعداد پیامهای خوانده نشده دریافت میگردد
export function getUnseenCount(nationalCode) {
  return ApiCall.wsCall(
    '/GetUnseenCount',
    {
      password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
      nationalCode: nationalCode,
    },
    CHAT_BASE_URL,
  )
}

// زمانی که پیام خوانده شد این متد برای ست کردن فیلد دیده شده در دیتابیس استفاده میشود
export function showMessage(nationalCode, msgId) {
  return ApiCall.wsCall(
    '/ShowMessage',
    {
      nationalCode: nationalCode,
      msgId: msgId,
      password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
    },
    CHAT_BASE_URL,
  )
}
export function setToken(nationalCode, token) {
  return ApiCall.wsCall(
    '/SetToken',
    {
      password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
      nationalCode: nationalCode,
      token: token,
    },
    CHAT_BASE_URL,
  )
}
export function searchMessage(nationalCode, phrase) {
  return ApiCall.wsCall(
    '/SearchMessage',
    {
      password: Utils.decoding('8dp5(7;kN-<78?AHnx5~wPH'),
      nationalCode: nationalCode,
      phrase: phrase,
    },
    CHAT_BASE_URL,
  )
}

// export function confirmMobileCode(mobile, code) {
//   return ApiCall.wsCall('/User/ConfirmSubmit', {
//     mobile: mobile,
//     code: code
//   });
// }

// export function login(mobile, password, fcmToken) {
//   return ApiCall.wsCall('/User/Login', {
//     mobile: mobile,
//     password: password,
//     notificationId: fcmToken
//   });
// }

// export function logout() {
//   return ApiCall.wsCall('/User/Logout');
// }

// export function getCodeToChangePassword(mobile) {
//   return ApiCall.wsCall('/User/GetCodeToChangePassword', {
//     mobile: mobile
//   });
// }

// export function confirmChangePassword(mobile, code) {
//   return ApiCall.wsCall('/User/ConfirmChangePassword', {
//     mobile: mobile,
//     code: code
//   });
// }

// export function changePassword(mobile, oldPassword, newPassword) {
//   return ApiCall.wsCall('/User/ChangePassword', {
//     mobile: mobile,
//     confirmCode: oldPassword,
//     Password: newPassword
//   });
// }

// export function setNotificationId(fcmToken) {
//   return ApiCall.wsCall('/Notify/SetNotificationId', {
//     data: fcmToken
//   });
// }

// export function getAppLastVersion() {
//   let deviceInfo = {
//     OS_Version: Platform.Version,
//     SystemVersion: DeviceInfo.getSystemVersion(),
//     Brand: DeviceInfo.getBrand(),
//     Manufacturer: DeviceInfo.getManufacturer(),
//     Model: DeviceInfo.getModel(),
//     OS_AppVersion: DeviceInfo.getVersion(),
//     RN_AppVersion: appVersion
//   };

//   return ApiCall.wsCall('/Utils/GetAppLastVersion', {
//     data: {
//       version: appVersion,
//       deviceInfo: JSON.stringify(deviceInfo)
//     }
//   });
// }

// export function editProfile(firstName, lastName, image, email, onUploadProgress = null) {
//   return ApiCall.wsCall(
//     '/User/EditProfile',
//     {
//       data: {
//         firstName: firstName,
//         lastName: lastName,
//         image: image,
//         email: email
//       }
//     },
//     onUploadProgress
//   );
// }

// export function getChatList() {
//   return ApiCall.wsCall('/Notify/GetMyMessageGroups');
// }

// export function getChatMessages(chatListId, lastId, afterMessageId, pageSize) {
//   return ApiCall.wsCall('/Notify/ReadChat', {
//     data: {
//       chatListId: chatListId,
//       fromMessageId: lastId,
//       afterMessageId: afterMessageId
//     },
//     pageSize: pageSize
//   });
// }

// export function sendMessage(myId, userId, chatListId, message) {
//   return ApiCall.wsCall('/Notify/SendMessage_ForApp', {
//     data: {
//       From: myId,
//       To: userId,
//       Text: message,
//       chatListId: chatListId
//     }
//   });
// }

// export function getMyOTPList() {
//   return ApiCall.wsCall('/OTP/GetUserOTPList');
// }

// export function getOTPList() {
//   return ApiCall.wsCall('/OTP/GetOTPList');
// }

// export function requestOTP(systemId) {
//   return ApiCall.wsCall('/OTP/RequestOTP', {
//     data: systemId
//   });
// }

// export function removeUserOTP(systemId) {
//   return ApiCall.wsCall('/OTP/RemoveUserOTP', {
//     data: systemId
//   });
// }

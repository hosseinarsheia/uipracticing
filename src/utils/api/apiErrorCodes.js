import R from '../../res/R'

const aeCodes = {
  WS_CANCEL: { ResultCode: -1000, ResultMessage: 'user canceled', isCanceled: true },
  WS_CONNECT_ERROR: { ResultCode: -1001, ResultMessage: R.strings.connectionErrorAndTip },
  WS_ERROR: { ResultCode: 1, ResultMessage: R.strings.webServiceError },
  LOGGED_OUT_ERROR: { ResultCode: 132, ResultMessage: R.strings.loggedOutError },
}

export default aeCodes

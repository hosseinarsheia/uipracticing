import { observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import jMoment from 'moment-jalaali'
import Storage from './Storage'
// import * as Api from './api'
import { Alert } from 'react-native'
import R from '../res/R'

/***************************** RootStore ***************************/
class RootStore {
  constructor() {}
}

export const rootStore = new RootStore()

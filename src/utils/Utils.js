import { View, Alert, AsyncStorage } from 'react-native'
import { default as NativeToast } from 'react-native-simple-toast'

import Storage from './Storage'
import Navigation from './Navigation'
import R from '../res/R'

/* function coding(text) {
  var r = "";
  for (let i = 0; i < text.length; i++) {
    r += String.fromCharCode(text.charCodeAt(i) + i);
  }

  return r;
}*/

function decoding(text) {
  var r = ''
  for (let i = 0; i < text.length; i++) {
    r += String.fromCharCode(text.charCodeAt(i) - i)
  }

  return r
}

function checkNationalCode(text) {
  try {
    if (text == null || text.length != 10 || toInt(text).length != text.length) return false

    var sum = 0

    for (let i = 0; i < 9; i++) {
      sum += text[i] * (10 - i)
    }

    let r = sum % 11
    if (r < 2) return text[9] == r
    else return text[9] == 11 - r
  } catch (err) {
    return false
  }
}

function toPersianNumber(text) {
  //۰۱۲۳۴۵۶۷۸۹
  text = text.toString()
  text = text
    .replace(/0/g, '۰')
    .replace(/1/g, '۱')
    .replace(/2/g, '۲')
    .replace(/3/g, '۳')
    .replace(/4/g, '۴')
    .replace(/5/g, '۵')
    .replace(/6/g, '۶')
    .replace(/7/g, '۷')
    .replace(/8/g, '۸')
    .replace(/9/g, '۹')

  return text
}

function toEnglishNumber(text) {
  //۰۱۲۳۴۵۶۷۸۹
  text = text.toString()
  text = text
    .replace(/۰/g, '0')
    .replace(/۱/g, '1')
    .replace(/۲/g, '2')
    .replace(/۳/g, '3')
    .replace(/۴/g, '4')
    .replace(/۵/g, '5')
    .replace(/۶/g, '6')
    .replace(/۷/g, '7')
    .replace(/۸/g, '8')
    .replace(/۹/g, '9')

  return text
}

function to3DigitSeparated(value = '') {
  num = parseInt(value) || 0
  let a = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return a
}

function toDecimal(text) {
  let newText = ''
  let numbers = '0123456789.'

  for (var i = 0; i < text.length; i++) {
    if (text.replace(/[^.]/g, '').length > 1 && text[i] == '.') {
    } else if (numbers.indexOf(text[i]) > -1) {
      newText = newText + text[i]
    }
  }

  if (newText.length > 1 && newText[0] == '0' && newText[1] >= '0' && newText[1] <= '9') newText = newText.substr(1)

  if (newText.length == 0 || newText === '') newText = '0'

  return newText
}

function toInt(text) {
  let newText = ''
  let numbers = '0123456789'

  for (var i = 0; i < text.length; i++) {
    if (numbers.indexOf(text[i]) > -1) {
      newText = newText + text[i]
    }
  }

  return newText
}

function onlyNumbers(text) {
  if (/^\d+$/.test(text)) return text
  else return text.substring(0, text.length - 1)
}

export default {
  decoding,
  checkNationalCode,
  toPersianNumber,
  toEnglishNumber,
  to3DigitSeparated,
  toDecimal,
  toInt,
  onlyNumbers,
  // coding
}

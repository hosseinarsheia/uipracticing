import { AsyncStorage } from 'react-native'

const storageName = 'NTSW'

const KEYS = {
  USER_INFO: 'userInfo',
}

async function saveItem(key, value) {
  try {
    await AsyncStorage.setItem(`@${storageName}:${key}`, value.toString())
    return true
  } catch (err) {
    return false
  }
}

async function getItem(key, def = '') {
  try {
    const value = await AsyncStorage.getItem(`@${storageName}:${key}`)
    return value !== null ? value : def
  } catch (error) {
    return def
  }
}

async function removeItem(key) {
  try {
    await AsyncStorage.removeItem(`@${storageName}:${key}`)
    return true
  } catch (err) {
    return false
  }
}

export default {
  KEYS,
  saveItem,
  getItem,
  removeItem,
}

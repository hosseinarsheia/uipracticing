import { StackActions, NavigationActions } from 'react-navigation'

const SCREENS = {
  SPLASH_SCREEN: 'SplashScreen',
  LOGIN_SCREEN: 'LoginScreen',
  MAIN_SCREEN: 'MainScreen',
}

const PARAMS = {}

let _navigator

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef
}

function convertParams(params) {
  let p = ''
  try {
    if (params) {
      p = '{'

      for (let i = 0; i < params.length; i++) {
        if (i > 0) p += ', '

        p += `"${params[i][0]}": `

        if (typeof params[i][1] === 'string') p += `"${params[i][1]}"`
        else if (typeof params[i][1] === 'object') p += JSON.stringify(params[i][1])
        else p += `${params[i][1]}`
      }

      p += '}'
      p = JSON.parse(p)
    } else {
      p = {}
    }
  } catch (err) {
    p = {}
  }

  return p
}

function navigate(name, ...params) {
  if (!_navigator) return

  _navigator.dispatch(
    NavigationActions.navigate({
      routeName: name,
      params: convertParams(params),
    }),
  )
}

function gotoScreen(names, ...params) {
  if (!_navigator) return

  var actions = []

  if (Array.isArray(names))
    for (let i = 0; i < names.length; i++) {
      if (i == names.length - 1)
        actions.push(NavigationActions.navigate({ routeName: names[i], params: convertParams(params) }))
      else actions.push(NavigationActions.navigate({ routeName: names[i] }))
    }
  else actions.push(NavigationActions.navigate({ routeName: names, params: convertParams(params) }))

  const resetAction = StackActions.reset({
    index: Array.isArray(names) ? names.length - 1 : 0,
    actions: actions,
  })
  _navigator.dispatch(resetAction)
}

function goBack(props) {
  props.navigation.goBack()
}

function getParam(props, paramName, def = '') {
  return props.navigation.getParam(paramName, def)
}

export default {
  SCREENS,
  PARAMS,
  setTopLevelNavigator,
  navigate,
  gotoScreen,
  goBack,
  getParam,
}
